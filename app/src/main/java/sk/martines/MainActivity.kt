package sk.martines

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import sk.martines.covid19App.R
import sk.martines.covid19App.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    val PIN = 1234
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.singInBtn.setOnClickListener {
            if (binding.pinEditText.text.toString() == PIN.toString()) {
                startActivity(Intent(this, Request::class.java))
                finish()
            } else {
                Toast.makeText(this, "Incorrect PIN", Toast.LENGTH_SHORT).show()
            }
        }

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (prefs.getBoolean("First opening", true)) {
            AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle("Terms of Use")
                .setMessage("By clicking OK, you agree to the terms of use of the application")
                .setPositiveButton("OK") {_, _ ->
                    prefs.edit().putBoolean("First opening", false).apply()
                }
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show()
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings ->
                run {
                    startActivity(Intent(this@MainActivity, Settings::class.java))
                    return true
                }
            else ->
                return super.onOptionsItemSelected(item)
        }
    }
}