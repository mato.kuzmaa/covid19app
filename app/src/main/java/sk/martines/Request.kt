package sk.martines

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import sk.martines.covid19App.databinding.RequestBinding
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.concurrent.ThreadLocalRandom

class Request:AppCompatActivity() {

    private lateinit var binding: RequestBinding
    var request = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = RequestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.requestBtn.setOnClickListener {
            if(!request) {
                request = true
                generateExaminationAppointment()
            } else {
                Toast.makeText(this, "The examination date has already been set !", Toast.LENGTH_SHORT).show()
            }
        }

        binding.logOutBtn.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
    fun generateExaminationAppointment() {
        val random = ThreadLocalRandom.current().nextInt(1, 7)
        val calendar = Calendar.getInstance()

        calendar.add(Calendar.DAY_OF_YEAR, random)

        val formatter = SimpleDateFormat("dd.MM.yyyy")
        val term = formatter.format(calendar.time)

        binding.examinationDateTxt.text = "Examination date : $term"
    }
}