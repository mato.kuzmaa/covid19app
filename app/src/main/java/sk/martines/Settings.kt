package sk.martines

import android.os.Bundle
import android.widget.RatingBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import sk.martines.covid19App.databinding.SettingsBinding

class Settings:AppCompatActivity() {

    private lateinit var binding: SettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = SettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Settings"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.ratingBar.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener{_, rateValue, _ ->
            Toast.makeText(this, "You rated the app on : $rateValue/5", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}